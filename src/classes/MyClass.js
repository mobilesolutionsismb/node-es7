
class MyClass {
	constructor(name) {
		this.name = name
	}

	sleep(time) {
		return new Promise(resolve => {
			setTimeout(() => {
				console.log('I awake from my slumber!')

				resolve()
			}, time)
		})
	}

	greet(friend) {
		console.log(`Hello ${friend}, I am ${this.name}!`)
	}
}

export default MyClass
