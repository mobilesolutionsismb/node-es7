import MyClass from './classes/MyClass'
import minimist from 'minimist'

const args = minimist(process.argv.slice(2))

async function doStuff() {
	const instance = new MyClass(args._[0])

	if (!args._[0] || !args._[1]) {
		console.log(`Usage: ${process.argv[1]} <Your Name> <A friend's name>`)
		process.exit()	
	}

	console.log('Sleeping a bit...')
	await instance.sleep(2000)

	instance.greet(args._[1])
}

doStuff()



