# NODE ES7 POC

## About

This is a quick, simple proof of concept that allows the use of ES7 constructs within NodeJS. It supports

- Static class propreties
- `async` and `await`
- ES6's module `import` and `export`
- Probably more :)

## Usage

After cloning run `npm install` to download the dependencies, then use

- `npm start` to run the code
- `npm run watch` to run the code using `nodemon` (every time a change is made, the application automagically restarts)
- `npm run build` to build a production version

Use this as a starting point to make NodeJS application using modern JavaScript. Enjoy! 


